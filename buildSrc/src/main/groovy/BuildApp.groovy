import org.gradle.api.DefaultTask
import org.gradle.api.file.*
import org.gradle.api.tasks.TaskAction

class BuildApp extends DefaultTask {

	BuildApp() {
		group = 'build'
		description = 'Collect JAR files in the workspace to assemble the application'
	}

	@TaskAction
	void action() {

		FileTree tree = project.fileTree(dir: '.')
		tree.include "**/libs/application-*.jar"

		project.copy { CopySpec spec ->
			spec.from tree.getFiles()
			spec.into project.buildDir.toPath()
		}

		project.configurations.felix.resolvedConfiguration.resolvedArtifacts.each { artifact ->
			project.copy { CopySpec spec ->
				spec.from artifact.file
				spec.into project.buildDir.toPath().resolve('lib')
			}
		}

		project.configurations.bundle.resolvedConfiguration.resolvedArtifacts.each { artifact ->
			project.copy { CopySpec spec ->
				spec.from artifact.file
				spec.into project.buildDir.toPath().resolve('bundle')
			}
		}

		project.configurations.bundlePlugin.resolvedConfiguration.resolvedArtifacts.each { artifact ->
			project.copy { CopySpec spec ->
				spec.from artifact.file
				spec.into project.buildDir.toPath().resolve('hot-plugins')
			}
		}
	}
}
