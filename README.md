# Summary
This project is composed by sub-projects divided in two major scopes

- A demo app: to illustrate the example API usage
- The API accompanied by its default implementation

> Task :projects
	
	------------------------------------------------------------
	Root project
	------------------------------------------------------------
	
	Root project 'tasks'
	\--- Project ':modules'
		 +--- Project ':modules:demo'
		 |    +--- Project ':modules:demo:application'
		 |    \--- Project ':modules:demo:console'
		 \--- Project ':modules:tasks'
			  +--- Project ':modules:tasks:api' 
			  \--- Project ':modules:tasks:impl'
          
The main component of this exercise is ':modules:tasks:api', where all API elements are defined.

Its default implementation is realized at ':modules:tasks:impl', where the abstract base implementations are defined,
along with the set of default concrete extensions.

The example app is a simple console, ':modules:demo:console',
which is only a component providing its services to the main application.

# Building the app

This repository comes with a copy of a Gradle wrapper, 
to facilitate building in any computer without an installation of this building system. 
A JDK for Java 8 is the only requirement.

The building logic is defined by Gradle build scripts and a micro plugin (at buildSrc) written in Groovy to encapsulate 
the definition of tasks, 
while illustrating the extensibility of the tool via plugins. 

Although, simple and locally defined, 
some projects are able to benefit from this flexibility, 
while bigger projects and enterprise systems could benefit more from projects dedicated to the creation of distributable and reusable plugins.

Simple steps:

To build the modules:
> ./gradlew build

To build the app itself:
> ./gradlew buildApp

* The application will be formed with its runtime, named Felix,
 along with the API and implementation as hot deployable plugins. 
 The building process builds the custom runtime as an embedded and transparent environment.
 
 To run the application, inside the build directory:
 > java -jar application-1.0-SNAPSHOT.jar

# Designing for the exercise

This exercise seeks to demonstrate in a tyne app a flexible and ready to grow architecture. 

Even though the trivial functionality could be realized in a small module with only Java as the base language,
this draft tries to illustrate the reality of current enterprise products where the ability to read and write 
polyglot projects, including multiple domain specific languages (DLS) is essential for growing endeavours.

For instance, it is not uncommon to see Gradle native DSL mixed with languages developed especially for a project/company, 
while Jenkins and other builders push to the usage of their own DSL in current trends.

Uniquely for exemplification, 
one can find under the meta directory of this project a collection of artifacts for different building phases.
A Jenkins, or other builders could use several scripts to build this project in different ways, 
including the usage of Gradle to go about building and organizing artifacts, or docker for distribution of a deliverable.

It is also interesting to notice that projects that grow in complexity overtime, 
normally involve the usage of a certain container to provide the runtime requirements. 
Tomcat and OSGi are examples of containers in the sense of this paragraph. 

To illustrate this aspect, this project uses OSGi as a runtime, due to its simplicity and its reliance on modularity. 
Tomcat can already be viewed in deployments of Artifactory, Jenkins and others..

Modularity is viewed here as a trail that also servers to illustrate the growing need for scopes and namespaces in multiple levels to allow applications
to grow while keeping control of logical and physical dependencies (in the sense of dependencies across distributable physical artifacts, such as JARs, in contrast with code).

Each of the modules of this example was designated to illustrate the separation of concerns, 
while cyclic dependencies are avoided in the class level, as traditionally done, but also on the package level and in the modular level. 
Leading to an application that can easily grow after the first effort for flexibility and even be updated in runtime, with minimum disturbance.

A concept better illustrated at: [Simple Reliability Analysis exercise](https://www.e-systems.tech/documents/51576/52651/Reliability+Analysis.pdf). 

Finally, this example also illustrates the manipulation of lower and higher levels of abstraction in a multi project enterprise set, 
inside the application, in its runtime and in its building process.


