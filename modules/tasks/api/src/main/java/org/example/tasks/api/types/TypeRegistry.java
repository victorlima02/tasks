package org.example.tasks.api.types;

import org.example.tasks.api.exception.NoSuchTypeException;
import org.osgi.annotation.versioning.ProviderType;

/**
 * Task type registry service interface.
 * <p>
 * By using registries, an application is able to consult task types and dynamically add new types.
 * This also provides a minimum interface for allowing dynamic components to be added in runtime.
 */
@ProviderType
public interface TypeRegistry {

	void addType( TaskType type );

	TaskType getByName( String name ) throws NoSuchTypeException;
}
