package org.example.tasks.api.manager;

import org.example.tasks.api.Task;
import org.osgi.annotation.versioning.ProviderType;

/**
 * Task factory service interface.
 * <p>
 * Implementations of this interface are required to map task types
 * provided as Strings to the corresponding task type classes and
 * build the resulting task as a new object.
 * <p>
 * Task type instances should be unique.
 */
@ProviderType
public interface TaskFactory {

	Task buildPersonalTask( String text );

	Task buildTask( String typeName, String text );

	Task buildWorkTask( String text );
}
