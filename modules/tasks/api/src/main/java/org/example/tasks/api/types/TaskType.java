package org.example.tasks.api.types;

/**
 * Task type represented as an interface.
 * <p>
 * By using an interface instead of a enumeration or simpler field types,
 * we are assuming that tasks types are an unlimited set that can grow as new requirements emerge
 * while allowing behaviour to be differentiated by type.
 * <p>
 * Hierarchy becomes a natural aspect provided by the language and composition is facilitated,
 * while new types can be appended in a plugin style (other jar files for instance).
 * <p>
 * This will also make other requirements a trivial task,
 * sorting for instance will only require comparators for simple streams.
 */
public interface TaskType extends Comparable< TaskType > {

	String getTypeName( );
}
