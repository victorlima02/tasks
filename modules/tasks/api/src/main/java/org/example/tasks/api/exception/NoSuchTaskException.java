package org.example.tasks.api.exception;

public class NoSuchTaskException extends RuntimeException {

	public NoSuchTaskException( String message ) {

		super( message );
	}
}
