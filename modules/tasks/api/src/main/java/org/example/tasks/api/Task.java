package org.example.tasks.api;

import org.example.tasks.api.types.TaskType;

/**
 * Basic task model, as a simple bean.
 */
public interface Task {

	long getId( );

	TasKStatus getStatus( );

	String getText( );

	TaskType getType( );

	void setId( long id );

	Task setStatus( TasKStatus status );

	Task setText( String text );

	Task setType( TaskType type );
}
