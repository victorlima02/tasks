package org.example.tasks.api.exception;

public class NoSuchTypeException extends RuntimeException {

	public NoSuchTypeException( String message ) {

		super( message );
	}
}
