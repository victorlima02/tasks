package org.example.tasks.api;

/**
 * Possible task states for a basic workflow.
 */
public enum TasKStatus {CREATED, IN_PROGRESS, COMPLETED}
