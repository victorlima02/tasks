package org.example.tasks.api.manager;

import org.example.tasks.api.Task;
import org.osgi.annotation.versioning.ProviderType;

import java.util.List;

/**
 * Facade for task manipulation as a service.
 * <p>
 * This facade also hides the need for clients to know
 * about types of data used to manipulate tasks.
 */
@ProviderType
public interface TaskManager {

	Task create( String type, String text );

	void delete( long id );

	Task getTaskById( long id );

	List< Task > list( );

	List< Task > listSortedByCreated( );

	List< Task > listSortedByStatus( );

	List< Task > listSortedByType( );

	String read( long id );

	void updateStatus( long id, String status );

	void updateText( long id, String text );

	void updateType( long id, String text );
}
