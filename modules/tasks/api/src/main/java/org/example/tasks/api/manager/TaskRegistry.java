package org.example.tasks.api.manager;

import org.example.tasks.api.Task;
import org.example.tasks.api.exception.NoSuchTaskException;
import org.osgi.annotation.versioning.ProviderType;

import java.util.List;

/**
 * Task registry service interface.
 */
@ProviderType
public interface TaskRegistry {

	Task addTask( Task task );

	void delete( long id );

	Task getById( long id ) throws NoSuchTaskException;

	List< Task > getAll( );
}
