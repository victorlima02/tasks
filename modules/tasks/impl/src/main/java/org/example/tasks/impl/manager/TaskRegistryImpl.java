package org.example.tasks.impl.manager;

import org.example.tasks.api.Task;
import org.example.tasks.api.exception.NoSuchTaskException;
import org.example.tasks.api.manager.TaskRegistry;
import org.osgi.service.component.annotations.Component;

import java.util.*;

@Component( immediate = true,
			service = TaskRegistry.class )
public class TaskRegistryImpl implements TaskRegistry {

	private long count;
	private final Map< Long, Task > tasks;

	public TaskRegistryImpl( ) {

		count = 0L;
		tasks = new HashMap<>( );
	}

	@Override
	public Task addTask( Task task ) {

		long id = count++;
		task.setId( id );
		tasks.putIfAbsent( id, task );
		return task;
	}

	@Override
	public void delete( long id ) {

		tasks.remove( id );
	}

	@Override
	public Task getById( long id ) {

		Task task = tasks.get( id );
		if ( task == null ) {
			throw new NoSuchTaskException( "id " + id );
		}
		return task;
	}

	@Override
	public List< Task > getAll( ) {

		return new ArrayList<>( tasks.values( ) );
	}
}
