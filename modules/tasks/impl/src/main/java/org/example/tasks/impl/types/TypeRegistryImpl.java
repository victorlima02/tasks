package org.example.tasks.impl.types;

import org.example.tasks.api.exception.NoSuchTypeException;
import org.example.tasks.api.types.*;
import org.osgi.service.component.annotations.Component;

import java.util.*;

@Component( immediate = true,
			service = TypeRegistry.class )
public class TypeRegistryImpl implements TypeRegistry {

	private final Map< String, TaskType > types;

	public TypeRegistryImpl( ) {

		types = new HashMap<>( );
		initBasicTypes( );
	}

	@Override
	public void addType( TaskType type ) {

		types.putIfAbsent( type.getTypeName( ), type );
	}

	@Override
	public TaskType getByName( String name ) {

		TaskType type = types.get( name );
		if ( type == null ) {
			throw new NoSuchTypeException( "name " + name );
		}
		return type;
	}

	private void initBasicTypes( ) {

		addType( new PersonalType( ) );
		addType( new WorkType( ) );
	}
}
