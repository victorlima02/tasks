package org.example.tasks.impl;

import org.example.tasks.api.*;
import org.example.tasks.api.types.TaskType;

/**
 * Base implementation for tasks and default concrete type.
 */
public class BaseTask implements Task {

	private long id;
	private String text;
	private TasKStatus status;
	private TaskType type;

	@Override
	public String toString( ) {

		return String.format( "[%d] (%s) (%s) %s", getId( ), getType( ), getStatus( ), getText( ) );
	}

	@Override
	public long getId( ) {

		return id;
	}

	@Override
	public TasKStatus getStatus( ) {

		return status;
	}

	@Override
	public String getText( ) {

		return text;
	}

	@Override
	public TaskType getType( ) {

		return type;
	}

	@Override
	public void setId( long id ) {

		this.id = id;
	}

	@Override
	public Task setStatus( TasKStatus status ) {

		this.status = status;
		return this;
	}

	@Override
	public Task setText( String text ) {

		this.text = text;
		return this;
	}

	@Override
	public Task setType( TaskType type ) {

		this.type = type;
		return this;
	}
}
