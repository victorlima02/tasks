package org.example.tasks.impl.manager;

import org.example.tasks.api.*;
import org.example.tasks.api.manager.*;
import org.example.tasks.api.types.*;
import org.osgi.service.component.annotations.*;

import java.util.List;

import static java.util.Comparator.*;
import static java.util.stream.Collectors.toList;

@Component( immediate = true,
			service = TaskManager.class )
public class TaskManagerImpl implements TaskManager {

	private volatile TaskFactory taskFactory;
	private volatile TaskRegistry taskRegistry;
	private volatile TypeRegistry typeRegistry;

	@Override
	public Task create( String type, String text ) {

		Task newTask = taskFactory.buildTask( type, text );
		return taskRegistry.addTask( newTask );

	}

	@Override
	public void delete( long id ) {

		taskRegistry.delete( id );
	}

	@Override
	public Task getTaskById( long id ) {

		return taskRegistry.getById( id );
	}

	@Override
	public List< Task > list( ) {

		return taskRegistry.getAll( );
	}

	@Override
	public List< Task > listSortedByCreated( ) {

		return taskRegistry.getAll( )
						   .stream( )
						   .sorted( comparingLong( Task::getId ) )
						   .collect( toList( ) );
	}

	@Override
	public List< Task > listSortedByStatus( ) {

		return taskRegistry.getAll( )
						   .stream( )
						   .sorted( comparing( Task::getStatus ) )
						   .collect( toList( ) );
	}

	@Override
	public List< Task > listSortedByType( ) {

		return taskRegistry.getAll( )
						   .stream( )
						   .sorted( comparing( Task::getType ) )
						   .collect( toList( ) );
	}

	@Override
	public String read( long id ) {

		return taskRegistry.getById( id )
						   .getText( );
	}

	@Override
	public void updateStatus( long id, String statusName ) {

		Task task = taskRegistry.getById( id );
		TasKStatus status = TasKStatus.valueOf( statusName.toUpperCase( ) );
		task.setStatus( status );
	}

	@Override
	public void updateText( long id, String text ) {

		Task task = taskRegistry.getById( id );
		task.setText( text );
	}

	@Override
	public void updateType( long id, String typeName ) {

		Task task = taskRegistry.getById( id );
		TaskType type = typeRegistry.getByName( typeName );
		task.setType( type );
	}

	@Reference
	public void setTaskFactory( TaskFactory factory ) {

		this.taskFactory = factory;
	}

	@Reference
	public void setTaskRegistry( TaskRegistry taskRegistry ) {

		this.taskRegistry = taskRegistry;
	}

	@Reference
	public void setTypeRegistry( TypeRegistry typeRegistry ) {

		this.typeRegistry = typeRegistry;
	}
}
