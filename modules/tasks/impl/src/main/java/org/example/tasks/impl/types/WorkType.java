package org.example.tasks.impl.types;

import org.example.tasks.impl.BaseTaskType;

public class WorkType extends BaseTaskType {

	public final static String WORK_TASK_TYPE_NAME = "work";

	public WorkType( ) {

		super( WORK_TASK_TYPE_NAME );
	}

}
