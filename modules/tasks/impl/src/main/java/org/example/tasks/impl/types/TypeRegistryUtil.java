package org.example.tasks.impl.types;

import org.example.tasks.api.types.*;
import org.osgi.service.component.annotations.*;

@Component( immediate = true )
public class TypeRegistryUtil {

	private static volatile TypeRegistry registry;

	public static void addType( TaskType type ) {

		getTypeRegistry( ).addType( type );
	}

	public static TaskType getByName( String name ) {

		return getTypeRegistry( ).getByName( name );
	}

	public static TypeRegistry getTypeRegistry( ) {

		return registry;
	}

	@Reference
	void setTypeRegistry( TypeRegistry registry ) {

		TypeRegistryUtil.registry = registry;
	}
}
