package org.example.tasks.impl;

import org.example.tasks.api.types.TaskType;

/**
 * Base abstract implementation for types.
 * <p>
 * According to this model,
 * types are required to be represented by a unique instance of
 * a specialized type, implementing TaskType.
 */
public abstract class BaseTaskType implements TaskType {

	private final String typeName;

	protected BaseTaskType( String typeName ) {

		this.typeName = typeName;
	}

	@Override
	public int compareTo( TaskType other ) {

		return getTypeName( ).compareTo( other.getTypeName( ) );
	}

	@Override
	public String toString( ) {

		return getTypeName( );
	}

	public String getTypeName( ) {

		return typeName;
	}
}
