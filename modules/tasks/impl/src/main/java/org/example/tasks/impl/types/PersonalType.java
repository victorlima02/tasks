package org.example.tasks.impl.types;

import org.example.tasks.impl.BaseTaskType;

public class PersonalType extends BaseTaskType {

	public final static String PERSONAL_TASK_TYPE_NAME = "personal";

	public PersonalType( ) {

		super( PERSONAL_TASK_TYPE_NAME );
	}
}
