package org.example.tasks.impl.manager;

import org.example.tasks.api.*;
import org.example.tasks.api.manager.TaskFactory;
import org.example.tasks.api.types.TaskType;
import org.example.tasks.impl.BaseTask;
import org.example.tasks.impl.types.TypeRegistryUtil;
import org.osgi.service.component.annotations.Component;

import static org.example.tasks.impl.types.PersonalType.PERSONAL_TASK_TYPE_NAME;
import static org.example.tasks.impl.types.WorkType.WORK_TASK_TYPE_NAME;

@Component( immediate = true,
			service = TaskFactory.class )
public class TaskFactoryImpl implements TaskFactory {

	@Override
	public Task buildPersonalTask( String text ) {

		return buildTask( PERSONAL_TASK_TYPE_NAME, text );
	}

	@Override
	public Task buildTask( String typeName, String text ) {

		Task task = new BaseTask( );
		TaskType type = TypeRegistryUtil.getByName( typeName );

		return task.setType( type )
				   .setText( text )
				   .setStatus( TasKStatus.CREATED );
	}

	@Override
	public Task buildWorkTask( String text ) {

		return buildTask( WORK_TASK_TYPE_NAME, text );
	}
}
