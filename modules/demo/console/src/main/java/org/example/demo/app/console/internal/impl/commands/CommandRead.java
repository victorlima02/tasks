package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static java.lang.System.out;

class CommandRead {

	private static void menu( ) {

		out.println( );
		out.println( "enter the task you wish to read: " );
	}

	static void execute( Scanner scanner, TaskManager taskManager ) {

		menu( );

		long id = scanner.nextLong( );
		scanner.nextLine( );

		out.println( taskManager.read( id ) );

	}
}
