package org.example.demo.app.console.internal.impl.commands;

import java.util.*;

import static java.lang.System.out;

class InputUtil {

	static String choseStatus( Scanner scanner ) {

		out.println( );
		out.println( "choose the status of your task" );
		out.println( "1. Created" );
		out.println( "2. In Progress" );
		out.println( "3. Completed" );

		int choice = getInt( scanner, 1, 3 );

		String status = null;

		switch ( choice ) {
			case 1:
				status = "created";
				break;

			case 2:
				status = "in_progress";
				break;

			case 3:
				status = "completed";
		}

		return status;
	}

	static String choseText( Scanner scanner ) {

		out.println( );
		out.println( "enter your task [1 line]: " );
		return scanner.nextLine( );
	}

	static String choseType( Scanner scanner ) {

		out.println( );
		out.println( "choose your task type" );
		out.println( "1. Personal" );
		out.println( "2. Work" );

		int choice = getInt( scanner, 1, 2 );

		return ( choice == 1 )
			   ? "personal"
			   : "work";
	}

	static int getInt( Scanner scanner, int lower, int higher ) {

		short choice = -1;
		boolean valid = false;

		do {

			try {

				choice = scanner.nextShort( );
				valid = choice >= lower && choice <= higher;

				if ( !valid ) {
					out.println( "invalid input - out of range \n" );
				}

			}
			catch ( InputMismatchException e ) {
				out.println( "invalid input - digits only \n" );
			}

			scanner.nextLine( );

		} while ( !valid );

		return choice;
	}

	static long getLong( Scanner scanner ) {

		long choice = -1;
		boolean valid = false;

		do {

			try {

				choice = scanner.nextLong( );
				valid = choice >= 0;

				if ( !valid ) {
					out.println( "invalid input - non negative numbers only \n" );
				}

			}
			catch ( InputMismatchException e ) {
				out.println( "invalid input - digits only \n" );
			}

			scanner.nextLine( );

		} while ( !valid );

		return choice;
	}
}

