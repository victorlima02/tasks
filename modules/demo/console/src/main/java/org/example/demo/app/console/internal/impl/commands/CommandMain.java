package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static java.lang.System.out;
import static org.example.demo.app.console.internal.impl.commands.CommandFacade.*;
import static org.example.demo.app.console.internal.impl.commands.InputUtil.getInt;

class CommandMain {

	private static void menu( ) {

		out.println( );
		out.println( "enter your choice" );
		out.println( "1. Create" );
		out.println( "2. Read" );
		out.println( "3. Update" );
		out.println( "4. Delete" );

		out.println( "5. List" );
		out.println( "6. Sort" );

		out.println( "7. exit" );
	}

	static void execute( Scanner scanner, TaskManager taskManager ) {

		boolean exit = false;

		do {
			menu( );
			int choice = getInt( scanner, 1, 7 );
			switch ( choice ) {
				case 1:
					create( scanner, taskManager );
					break;

				case 2:
					read( scanner, taskManager );
					break;

				case 3:
					update( scanner, taskManager );
					break;

				case 4:
					delete( scanner, taskManager );
					break;

				case 5:
					print( scanner, taskManager );
					break;

				case 6:
					sort( scanner, taskManager );
					break;

				case 7:
					exit = true;
					break;
			}

		} while ( !exit );

	}
}
