package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.Task;
import org.example.tasks.api.manager.TaskManager;

import java.util.*;

import static java.lang.System.out;

class CommandPrintTasks {

	static void execute( Scanner scanner, TaskManager taskManager ) {

		printTasks( taskManager.list( ) );
	}

	static void printTasks( List< Task > tasks ) {

		tasks.forEach( out::println );
	}
}
