package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.exception.NoSuchTaskException;
import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static java.lang.System.out;
import static org.example.demo.app.console.internal.impl.commands.InputUtil.*;

class CommandUpdate {

	private static void menu( ) {

		out.println( );
		out.println( "enter the task you wish to update: " );
	}

	private static void subMenu( ) {

		out.println( "type of change: " );
		out.println( "1. Type" );
		out.println( "2. Text" );
		out.println( "3. Status" );
	}

	static void execute( Scanner scanner, TaskManager taskManager ) {

		menu( );

		try {

			long id = getLong( scanner );

			subMenu( );

			int choice = getInt( scanner, 1, 3 );

			switch ( choice ) {
				case 1:
					String type = choseType( scanner );
					taskManager.updateType( id, type );
					break;

				case 2:
					String text = choseText( scanner );
					taskManager.updateText( id, text );
					break;

				case 3:
					String status = choseStatus( scanner );
					taskManager.updateStatus( id, status );
					break;
			}
		}
		catch ( NoSuchTaskException e ) {
			out.println( "task not found\n" );
		}

	}

}
