package org.example.demo.app.console;

import org.example.tasks.api.manager.TaskManager;
import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface Console {

	void run( );

	void setTaskManager( TaskManager taskManager );
}
