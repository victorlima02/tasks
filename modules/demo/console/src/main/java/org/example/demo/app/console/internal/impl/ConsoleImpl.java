package org.example.demo.app.console.internal.impl;

import org.example.demo.app.console.Console;
import org.example.tasks.api.manager.TaskManager;
import org.osgi.service.component.annotations.*;

import java.util.Scanner;

import static java.lang.System.in;
import static org.example.demo.app.console.internal.impl.commands.CommandFacade.main;

@Component( immediate = true,
			service = Console.class )
public class ConsoleImpl implements Console {

	private TaskManager taskManager;

	public void run( ) {

		final Scanner scanner = new Scanner( in );
		main( scanner, taskManager );
	}

	@Override
	@Reference
	public void setTaskManager( TaskManager taskManager ) {

		this.taskManager = taskManager;
	}

}
