package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

public interface CommandFacade {

	static void create( Scanner scanner, TaskManager taskManager ) {

		CommandCreate.execute( scanner, taskManager );
	}

	static void delete( Scanner scanner, TaskManager taskManager ) {

		CommandDelete.execute( scanner, taskManager );
	}

	static void main( Scanner scanner, TaskManager taskManager ) {

		CommandMain.execute( scanner, taskManager );
	}

	static void print( Scanner scanner, TaskManager taskManager ) {

		CommandPrintTasks.execute( scanner, taskManager );
	}

	static void read( Scanner scanner, TaskManager taskManager ) {

		CommandRead.execute( scanner, taskManager );
	}

	static void sort( Scanner scanner, TaskManager taskManager ) {

		CommandSort.execute( scanner, taskManager );
	}

	static void update( Scanner scanner, TaskManager taskManager ) {

		CommandUpdate.execute( scanner, taskManager );
	}
}
