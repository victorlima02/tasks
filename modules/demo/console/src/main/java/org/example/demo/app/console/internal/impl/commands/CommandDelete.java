package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static java.lang.System.out;
import static org.example.demo.app.console.internal.impl.commands.InputUtil.getLong;

class CommandDelete {

	private static void menu( ) {

		out.println( );
		out.println( "enter the task you wish to delete: " );
	}

	static void execute( Scanner scanner, TaskManager taskManager ) {

		menu( );

		long id = getLong( scanner );

		taskManager.delete( id );

	}
}
