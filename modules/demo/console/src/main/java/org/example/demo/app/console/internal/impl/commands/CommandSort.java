package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static java.lang.System.out;
import static org.example.demo.app.console.internal.impl.commands.CommandPrintTasks.printTasks;
import static org.example.demo.app.console.internal.impl.commands.InputUtil.getInt;

class CommandSort {

	private static void menu( ) {

		out.println( );
		out.println( "select the type of sorting: " );
		out.println( "1. By creation order" );
		out.println( "2. By type" );
		out.println( "3. By Status" );
	}

	static void execute( Scanner scanner, TaskManager taskManager ) {

		menu( );

		int choice = getInt( scanner, 1, 3 );

		switch ( choice ) {
			case 1:
				printTasks( taskManager.listSortedByCreated( ) );
				break;

			case 2:
				printTasks( taskManager.listSortedByType( ) );
				break;

			case 3:
				printTasks( taskManager.listSortedByStatus( ) );
				break;
		}
	}
}
