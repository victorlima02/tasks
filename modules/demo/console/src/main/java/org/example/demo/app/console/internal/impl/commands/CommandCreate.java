package org.example.demo.app.console.internal.impl.commands;

import org.example.tasks.api.manager.TaskManager;

import java.util.Scanner;

import static org.example.demo.app.console.internal.impl.commands.InputUtil.*;

class CommandCreate {

	static void execute( Scanner scanner, TaskManager taskManager ) {

		String description = choseText( scanner );

		String type = choseType( scanner );

		taskManager.create( type, description );

	}

}
