package org.example.tasks.demo.app;

import org.apache.felix.main.AutoProcessor;
import org.osgi.framework.*;
import org.osgi.framework.launch.*;

import java.lang.reflect.*;
import java.util.*;

class FelixRuntime {

	private static Framework felix;
	private static String CONSOLE_SERVICE = "org.example.demo.app.console.Console";

	FelixRuntime( ) {

		loadFelix( );
	}

	public void stop( ) {

		try {
			if ( felix != null ) {
				felix.stop( );
				felix.waitForStop( 0 );
			}
		}
		catch ( Exception ex ) {
			System.err.println( "Error stopping framework: " + ex.getMessage( ) );
			ex.printStackTrace( );
			System.exit( -1 );
		}
	}

	private void addShutdownHook( ) {

		Runtime.getRuntime( )
			   .addShutdownHook( new Thread( "Felix Shutdown Hook" ) {

				   public void run( ) {

					   try {
						   if ( felix != null ) {
							   felix.stop( );
							   felix.waitForStop( 0 );
						   }
					   }
					   catch ( Exception ex ) {
						   System.err.println( "Error stopping framework: " + ex );
					   }
				   }
			   } );
	}

	private void loadFelix( ) {

		Map< String, String > configMap = new HashMap<>( );
		configMap.put( AutoProcessor.AUTO_DEPLOY_DIR_PROPERTY, "bundle" );
		configMap.put( AutoProcessor.AUTO_DEPLOY_ACTION_PROPERTY, "install,start" );
		configMap.put( Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit" );
		configMap.put( "felix.fileinstall.dir", "hot-plugins" );
		configMap.put( "felix.log.level", "2" );

		try {
			FrameworkFactory factory = getFrameworkFactory( );
			felix = factory.newFramework( configMap );
			felix.init( );
			AutoProcessor.process( configMap, felix.getBundleContext( ) );
			felix.start( );
			addShutdownHook( );
		}
		catch ( Exception ex ) {
			System.err.println( "Could not create framework: " + ex.getMessage( ) );
			ex.printStackTrace( );
			System.exit( -1 );
		}
	}

	private FrameworkFactory getFrameworkFactory( ) {

		ServiceLoader< FrameworkFactory > factoryServiceLoader = ServiceLoader.load( FrameworkFactory.class );
		return factoryServiceLoader.iterator( )
								   .next( );
	}

	void startConsoleService( ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

		BundleContext context = felix.getBundleContext( );

		ServiceReference serviceReference;

		do {
			serviceReference = context.getServiceReference( CONSOLE_SERVICE );
		} while ( serviceReference == null );

		Object service = context.getService( serviceReference );

		Method run = service.getClass( )
							.getMethod( "run" );

		run.invoke( service );
	}
}
