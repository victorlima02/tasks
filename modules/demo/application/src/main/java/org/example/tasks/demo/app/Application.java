package org.example.tasks.demo.app;

import java.lang.reflect.InvocationTargetException;

public class Application {

	public static void main( String[] args ) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

		FelixRuntime runtime = new FelixRuntime( );
		try {
			runtime.startConsoleService( );
		}
		finally {
			runtime.stop( );
		}
	}
}
